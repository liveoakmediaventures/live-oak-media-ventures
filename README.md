Email Marketing veterans with over 20 years of online marketing experience. We specialize in list management and monetization, with a focus on the following verticals:

*Finance (including Mortgage, Subprime, etc)
*Homeowner Services
*Education
*Jobs
*Health
*Consumer

Please contact us for data management and other monetization opportunities.

Website : https://liveoakmediaventures.com